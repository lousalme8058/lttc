<footer class="patSection patFooArea js-footerY">
	<div class="patFoo--decLine"></div>
	<!-- info -->
	<div class="patfooMainArea">
		<section class="patfooMainArea-socialArea">
			<h4 class="patfooMainArea-socialArea--h4">Follow us</h4>
			<a href="javascript:void(0);" title="撥打電話聯絡小光點畫廊" class="patfooMainArea-socialArea--bt patfooMainArea-socialArea--bt--phone" onclick="location.href='tel:0906956005'">
				<img src="images/newphone-bt.svg" alt="撥打電話聯絡小光點畫廊" class="patfooMainArea-socialArea--bt--icon">
				<span class="patfooMainArea-socialArea--bt--layer"></span>
			</a>
			<a href="https://www.facebook.com/spotlightgallerytw" title="小光點畫廊facebook粉絲團" class="patfooMainArea-socialArea--bt" target="_blank">
				<img src="images/newfb-bt.svg" alt="小光點畫廊facebook粉絲團" class="patfooMainArea-socialArea--bt--icon">
				<span class="patfooMainArea-socialArea--bt--layer"></span>
			</a>
			<a href="https://lin.ee/uEGxKew" title="小光點畫廊line官方帳號" class="patfooMainArea-socialArea--bt" target="_blank">
				<img src="images/newline-bt.svg" alt="小光點畫廊line官方帳號" class="patfooMainArea-socialArea--bt--icon">
				<span class="patfooMainArea-socialArea--bt--layer"></span>
			</a>
		</section>
		<img src="images/newfooLogo.svg" alt="logo" class="patfooMainArea-logo">
		<h4 class="patfooMainArea-copyright">
			Copyright © 2020 Spotlight Gallery
		</h4>
	</div>
	<!-- 合作夥伴 -->
	<div class="patfooRelateArea">
		<a href="https://www.facebook.com/drinkingteatw/" class="patfooRelateLink" title="禾禾茶"  target="_blank">
			<img src="images/newlinks01.png" alt="禾禾茶" class="" width="auto" height="auto">
		</a>
		<a href="http://www.ceramic.url.tw/" class="patfooRelateLink" title="陶緣彩瓷"  target="_blank">
			<img src="images/newlinks02.png" alt="陶緣彩瓷" class="" width="auto" height="auto">
		</a>
		<a href="http://www.tfrd.org.tw/tfrd/" class="patfooRelateLink" title="罕見疾病基金會"  target="_blank">
			<img src="images/newlinks03.png" alt="罕見疾病基金會" class="" width="auto" height="auto">
		</a>
	</div>
	<p class="patfooSlogan">
		<span class="patfooSlogan-text">凝聚光點，照亮世界</span>
	</p>
	<h4 class="patfooDesign">
		<a href="https://cosmosdesign.tw/" class="patfooDesign-text" title="網頁設計公司:小宇宙資訊">Design by COSMOS</a>
	</h4>
</footer>