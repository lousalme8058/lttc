<!-- 電腦版header -->
<header class="patheaderBk js-patheaderBk">
<a href="index.php" class="patheaderBk-logo" title="">
		<img src="images/logo.svg" alt="語言訓練測驗中心LOGO" class="">
	</a>
	<div class="patheaderBk-linkBk">
		<a href="exhibits.php" class="patheaderBk-linkBk--link mr-60">
			<span class="ch">線上特展</span>
			<span class="en">e-Exhibits</span>
		</a>
		
		<div class="patheaderBk-linkBk--link mr-60">
			<div class="dropdown">
				<a href="" class="dropdown-tit">
					<span class="ch">重點活動</span>
					<span class="en">Events</span>
				</a>
				<!-- The content -->
				<div class="dropdown__content plr-20 pt-40">
					<a href="opening.php" class="dropdown__content_link">
						<span class="ch-content pb-20">慶祝茶會</span>
						<span class="en-content pb-20">Opening Ceremony</span>
					</a>
					<a href="opening.php#js-pagOpSection02-ahref" class="dropdown__content_link pb-20">
						<span class="ch-content pb-20">現場展覽</span>
						<span class="en-content pb-20">Exhibition Gallery</span>
					</a>
				</div>
			</div>
		</div>

		<a href="wish.php" class="patheaderBk-linkBk--link mr-60">
			<span class="ch">各方祝福</span>
			<span class="en">Well-wishers</span>
		</a>

		<a href="https://www.lttc.ntu.edu.tw/en-trends" target="_blank" class="patheaderBk-linkBk--link mr-60">
			<span class="ch">學外語抽好禮</span>
			<span class="en">Prize Draw</span>
		</a>	
	</div>
</header>
