<!DOCTYPE html>	
<head>
<title>LTTC70週年慶網站</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>

<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    // ScrollTrigger.saveStyles(".indBanner-main");

    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {
        // gsap.to(".js-loadingCircle01", {
        //     scrollTrigger: {
        //         trigger: ".js-loadingCircle01",
        //         start: "top center",
        //         toggleActions: "play pause resume none",
        //         scrub: true,
        //         markers: true,
        //     },
        //     x: 500,
        //     duration: 2.5, 
        //     rotation: 360, 
        //     //scrollTrigger和repeat一起使用會發生定位問題
        //     // repeat: 5,
        // });
        // gsap.to(".js-loadingCircle02", {
        //     scrollTrigger: {
        //         trigger: ".js-loadingCircle02",
        //         start: "top center",
        //         /**/
        //         toggleActions: "restart pause resume pause",
        //         scrub: false,
        //         markers: true,
        //     },
        //     x: 500,
        //     duration: 3,
        //     rotation: 360,
        //     scale: 0.5,
        //     // repeat: 2,
        // });
      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
        // ScrollTriggers created here aren't associated with a particular media query,
        // so they persist.
        // gsap.to(".js-pagExLinkBk-01", {
        //     scrollTrigger: {
        //         trigger: ".js-pagExLinkBk-01",
        //         start: "top center",
        //         // endTrigger: ".js-indAct-link03",
        //         // end: "bottom top",
        //         toggleActions: "play pause none none",
        //         scrub: true,
        //         markers: true,
        //         /*到strat定位只執行一次*/
        //         once: true,
        //     },
        //     delay: 1,
        //     // opacity: 1,
        //     x: 100,
        //     ease: {ease: Power3.easeInOut, y: 0 },
        // });
        gsap.to(".js-pagExLinkBk-01", {
            delay: 1,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        gsap.to(".js-pagExLinkBk-02", {
            delay: 2,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        gsap.to(".js-pagExLinkBk-03", {
            scrollTrigger: {
                trigger: ".js-pagExLinkBk-03",
                start: "top center",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                once: true,
            },
            delay: 1,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        gsap.to(".js-pagExLinkBk-04", {
            scrollTrigger: {
                trigger: ".js-pagExLinkBk-04",
                start: "top center",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                once: true,
            },
            delay: 1,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        gsap.to(".js-pagExLinkBk-05", {
            scrollTrigger: {
                trigger: ".js-pagExLinkBk-05",
                start: "top center",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                once: true,
            },
            delay: 1,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        gsap.to(".js-pagExLinkBk-06", {
            scrollTrigger: {
                trigger: ".js-pagExLinkBk-06",
                start: "top center",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                once: true,
            },
            delay: 1,
            opacity: 1,
            x: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="">
    <?php require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="of-hidden">
        <!-- tit -->
        <div class="pagExPagetitBk">
            <h1 class="pagExPagetit">
                線上特展<br />
                e-Exhibits
            </h1>
        </div>

        <!-- 從代工到自創品牌 From OEM To OBM -->
        <div class="pagExLinkBk pagExLinkBk-01 js-pagExLinkBk-01">
            <div class="max_width">
                <a href="exhibitsin01.php" class="pagExLink">
                    <img src="images/exlink01.png" alt="從代工到自創品牌" class="pagExLink-img">
                    <div class="pagExLink-LinktitBk pr-60">
                        <h3 class="pagExLink-Link">從代工到自創品牌</h3>
                        <h3 class="pagExLink-Link">From OEM To OBM</h3>
                    </div>
                    <img src="images/exlink01-sml.png" alt="從代工到自創品牌" class="pagExLink-img--sml">
                </a>
            </div>
        </div>

        <!-- 立足台灣放眼世界 From Taiwan To the World -->
        <div class="pagExLinkBk pagExLinkBk-02 js-pagExLinkBk-02">
            <div class="max_width">
                <a href="exhibitsin02.php" class="pagExLink">
                    <div class="pagExLink-LinktitBk pl-60">
                        <h3 class="pagExLink-Link">立足臺灣放眼世界</h3>
                        <h3 class="pagExLink-Link">From Taiwan To the World</h3>
                    </div>
                    <img src="images/exlink02.png" alt="立足台灣放眼世界" class="pagExLink-img">
                    <img src="images/exlink02-sml.png" alt="立足台灣放眼世界" class="pagExLink-img--sml">
                </a>
            </div>
        </div>

        <!-- 終身學習好夥伴 Your Lifelong Learning Partner​ -->
        <div class="pagExLinkBk pagExLinkBk-03 js-pagExLinkBk-03">
            <div class="max_width">
                <a href="exhibitsin04.php" class="pagExLink">
                    <img src="images/exlink03.png" alt="終身學習好夥伴" class="pagExLink-img">
                    <div class="pagExLink-LinktitBk pr-60">
                        <h3 class="pagExLink-Link">終身學習好夥伴</h3>
                        <h3 class="pagExLink-Link">Your Lifelong Learning Partner​</h3>
                    </div>
                    <img src="images/exlink03-sml.png" alt="終身學習好夥伴" class="pagExLink-img--sml">
                </a>
            </div>
        </div>

        <!-- 趨向多元服務 Your Lifelong Learning Partner​ -->
        <div class="pagExLinkBk pagExLinkBk-04 js-pagExLinkBk-04">
            <div class="max_width">
                <a href="exhibitsin03.php" class="pagExLink">
                    <div class="pagExLink-LinktitBk pl-60">
                        <h3 class="pagExLink-Link">趨向多元服務</h3>
                        <h3 class="pagExLink-Link">Towards Diversified Services​</h3>
                    </div>
                    <img src="images/exlink04.png" alt="趨向多元服務" class="pagExLink-img">
                    <img src="images/exlink04-sml.png" alt="趨向多元服務" class="pagExLink-img--sml">
                </a>
            </div>
        </div>

        <!-- 從傳統到數位 From traditional to digital​​ -->
        <div class="pagExLinkBk pagExLinkBk-05 js-pagExLinkBk-05">
            <div class="max_width">
                <a href="exhibitsin05.php" class="pagExLink">
                    <img src="images/exlink05.png" alt="從傳統到數位" class="pagExLink-img">
                    <div class="pagExLink-LinktitBk pr-60">
                        <h3 class="pagExLink-Link">從傳統到數位</h3>
                        <h3 class="pagExLink-Link">From Traditional to Digital</h3>
                    </div>
                    <img src="images/exlink05-sml.png" alt="從傳統到數位" class="pagExLink-img--sml">
                </a>
            </div>
        </div> 

        <!-- 教育公益 Education & Public Welfare​​​ -->
        <div class="pagExLinkBk pagExLinkBk-06 js-pagExLinkBk-06">
            <div class="max_width">
                <a href="exhibitsin06.php" class="pagExLink">
                    <div class="pagExLink-LinktitBk pl-60">
                        <h3 class="pagExLink-Link">教育公益</h3>
                        <h3 class="pagExLink-Link">Educational Service for the Public</h3>
                    </div>
                    <img src="images/exlink06.png" alt="教育公益" class="pagExLink-img">
                    <img src="images/exlink06-sml.png" alt="教育公益" class="pagExLink-img--sml">
                </a>
            </div>
        </div> 

        <!-- 首頁底元素 -->
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
        </div>
    </div>

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     