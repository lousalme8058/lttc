<!DOCTYPE html>	
<head>
<title>LTTC70週年慶網站</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>

<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'flipInX',
        loop: true,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 300,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1024: {
                items: 2
            },
            1280: {
                items: 2,
                margin: 20,
            },
            1860:{
                items: 3,
                margin: 20,
            }
        }
    });
});

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    // ScrollTrigger.saveStyles(".indBanner-main");

    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1280px)": function() {
        //banner區
        gsap.to(".indBanner-robot", {
            y: -10,
            repeat: -1,
            delay: 3,
            duration: 2,
            ease: {ease: Power3.easeInOut, y: 0 },
            yoyo: true,
        });
        //影片連結輪播區
        gsap.to(".indVideoLink-bg--img", {
            x: -5732,
            repeat: -1,
            delay: 1,
            duration: 250,
            ease: { ease: "none", y: -500 },
            yoyo: true,
        });
        //重點活動
        gsap.to(".indAct-link", {
            scrollTrigger: {
                trigger: ".indAct-tit",
                start: "top top+=250",
                endTrigger: ".indAct-link",
                end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            opacity: 1,
            y: -30,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
    },
  
    // mobile
    "(max-width: 768px)": function() {
        //影片連結輪播區
        gsap.to(".indVideoLink-bg--img", {
            x: -2099,
            repeat: -1,
            delay: 1,
            duration: 200,
            ease: { ease: "none", y: -500 },
            yoyo: true,
        });

        
    },
      
    // all 
    "all": function() {
      // ScrollTriggers created here aren't associated with a particular media query,
      // so they persist.
        //banner區
        var tl = gsap.timeline();
        tl.to(".indBanner-main",{
            x: -20,
            opacity: 0,
        })
        tl.to(".indBanner-main", {
            x: 0,
            duration: 2, 
            opacity: 1,
            delay: 2.5,
        });
        tl.to(".indBanner-main", {
            y: 8,
            repeat: -1,
            delay: 2,
            duration: 2.5,
            ease: {ease: Power4.easeInOut, y: 0 },
            yoyo: true,
        });

        var tl02 = gsap.timeline();
        tl02.to(".indBanner-titBk--chtit",{
            x: 40,
            opacity: 0,
        })
        tl02.to(".indBanner-titBk--chtit", {
            x: 0,
            duration: 1.5, 
            opacity: 3,
            delay: 2,
        });
        tl02.to(".indBanner-titBk--entit", {
            y: 10,
            repeat: -1,
            delay: 2,
            duration: 2.5,
            ease: {ease: Power4.easeInOut, y: 0 },
            yoyo: true,
        });

        gsap.to(".indBanner-bg02", {
            y: 7,
            repeat: -1,
            delay: 1.5,
            duration: 3.5,
            ease: {ease: Power3.easeInOut, y: 0 },
            yoyo: true,
        });
        gsap.to(".indBanner-girl", {
            y: -10,
            repeat: -1,
            delay: 4,
            duration: 2.5,
            ease: {ease: Power1.easeInOut, y: 0 },
            yoyo: true,
        });
        gsap.to(".indBanner-machine", {
            y: 10,
            x: -20,
            repeat: -1,
            delay: 2.5,
            duration: 2.5,
            ease: {ease: Power3.easeInOut, y: 0 },
            yoyo: true,
        });

        // 大事記區
        gsap.to(".indMemoBk--hand", {
            opacity: 1, 
            x: 20,
            duration: 1,
            repeat: -1 ,
            ease: {ease: Power3.easeInOut, y: 0 },
            yoyo: true,
        });
        
        var tl03 = gsap.timeline();
        tl03.to(".indMemoBk--layer", {
            scrollTrigger: {
                trigger: ".indMemo-tit",
                start: "top top+=250",
                endTrigger: ".indMemo-tit",
                end: "bottom+=100 top",
                toggleActions: "play pause none none",
                // pin: true,
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
    	    // position: "fixed",
            opacity: 1,
            zIndex: 10,
            // duration: 3,
            // repeat: -1,
        	// top: 0,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        $(".indMemoBk--layer").click(function(){
            $(".indMemoBk--layer").css("opacity","0");
            $(".indMemoBk--layer").css("z-index","-9999");
        });     

        var tl04 = gsap.timeline();
        tl04.to(".js-indAct-link01", {
            scrollTrigger: {
                trigger: ".indAct-tit",
                start: "top center-=120",
                endTrigger: ".js-indAct-link03",
                end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            opacity: 1,
            y: -20,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        tl04.to(".js-indAct-link02", {
            scrollTrigger: {
                trigger: ".indAct-tit",
                start: "top center-=120",
                endTrigger: ".js-indAct-link03",
                end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            delay: 0.6,
            opacity: 1,
            y: -20,
            ease: {ease: Power3.easeInOut, y: 0 },
        });
        tl04.to(".js-indAct-link03", {
            scrollTrigger: {
                trigger: ".indAct-tit",
                start: "top center-=120",
                endTrigger: ".js-indAct-link03",
                end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            delay: 0.6,
            opacity: 1,
            y: -20,
            ease: {ease: Power3.easeInOut, y: 0 },
        });   
        
    }
      
  }); 
});

$(window).on('load',function(){

});

// Horizontal Click and Drag Scrolling with JS - Prevent click on mouseup
//https://stackoverflow.com/questions/58788955/horizontal-click-and-drag-scrolling-with-js-prevent-click-on-mouseup
$(document).ready(function() { 
    const slider = document.querySelector('.indMemo');
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('mousedown', (e) => {
        isDown = true;
        slider.classList.add('js-active');
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });

    slider.addEventListener('mouseleave', () => {
        isDown = false;
        slider.classList.remove('js-active');
    });

    slider.addEventListener('mouseup', () => {
        isDown = false;
        slider.classList.remove('js-active');
    });

    slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 2; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
        // console.log(walk);
    });
});
</script>

<body class="indBg">

    <?php require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <!-- banner動畫區 -->
    <div class="indBanner">
        <!-- tit -->
        <div class="indBanner-titBk">
            <img src="images/indbanner07.png" alt="LTTC" class="indBanner-titBk--entit">
            <br />
            <img src="images/indbanner08.png" alt="跨出自信，世界同行" class="indBanner-titBk--chtit">
        </div>
        <!-- 地球 -->
        <div class="indBanner-mainBk">
            <img src="images/indbanner03.png" alt="地球" class="indBanner-main">
        </div>
        <!-- 背景圖層元素 -->
        <img src="images/indbanner02.png" alt="藍雲" class="indBanner-bg01">
        <img src="images/indbanner01.png" alt="雲+男孩" class="indBanner-bg02">
        <img src="images/indbanner09.png" alt="白雲" class="indBanner-bg03">
        <!-- 其他 -->
        <img src="images/indbanner05.png" alt="女孩" class="indBanner-girl">
        <img src="images/indbanner04.png" alt="無人機" class="indBanner-machine">
        <img src="images/indbanner06.png" alt="機器人" class="indBanner-robot">
        <!-- <div class="indBanner-bg"></div> -->
    </div>

    <!-- 祝賀輪播 -->
    <?php require('indVideoBanner.php') ?>

    <!-- 大事記 -->
    <?php require('indMemo.php') ?>

    <!-- 重點活動 -->
    <div class="indAct">
        <!-- tit -->
        <div class="indAct-titBk">
            <div class="indAct-tit">
                <h5>重點活動</h5>
                <h6>Events</h6>
            </div>
        </div>

        <!-- link -->
        <div class="indAct-actLinkBk">
            <!-- 慶祝茶會 -->
            <a href="opening.php" class="indAct-link js-indAct-link01">
                <img src="images/act01.png" alt="慶祝茶會" class="indAct-link--img">
                <div class="indAct-link--titBk">
                    <h6>慶祝茶會</h6>
                    <h6>Opening Ceremony</h6>
                </div>
            </a>
            <!-- 現場展覽 -->
            <a href="opening.php" class="indAct-link js-indAct-link02">
                <img src="images/act02.png" alt="現場展覽" class="indAct-link--img indAct-link--img-big">
                <div class="indAct-link--titBk">
                    <h6>現場展覽</h6>
                    <h6>Exhibition Gallery</h6>
                </div>
            </a>
            <!-- 學外語抽好禮 -->
            <a href="https://www.lttc.ntu.edu.tw/en-trends" target="_blank" class="indAct-link js-indAct-link03">
                <img src="images/act03.png" alt="學外語抽好禮" class="indAct-link--img">
                <div class="indAct-link--titBk">
                    <h6>學外語抽好禮 6/1~8/31</h6>
                    <h6>Prize Draw</h6>
                </div>
            </a>
        </div>
    </div>

    <!-- 首頁底元素 -->
    <div class="indBottomEleBk">
        <img src="images/indele10.png" alt="element" class="indBottomEle01">
        <img src="images/indbanner04.png" alt="element" class="indBottomEle02">
        <img src="images/indele08.png" alt="element" class="indBottomEle03">
        <img src="images/indele09.png" alt="element" class="indBottomEle04">
    </div>

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     