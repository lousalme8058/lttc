<!DOCTYPE html>	
<head>
<title>LTTC70週年慶網站</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagExin04">
    <div class="pagExin04--bg">
        <img src="images/psgEx4-12.svg" alt="" class="pagExin04--bg01">
        <img src="images/psgEx4-14.svg" alt="" class="pagExin04--bg02">
        <img src="images/psgEx4-13.svg" alt="" class="pagExin04--bg03">
        <img src="images/psgEx4-12.svg" alt="" class="pagExin04--bg04">
    </div>
    <?php require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin04-bannerBk">
        <div class="pagExin04-pageTitBk">
            <h1 class="">
                終身學習好夥伴
                <br>
                Your Lifelong <br> Learning Partner​
            </h1>
        </div>
        <img src="images/psgEx4-02.png" alt="" class="pagExin04-banner">
    </div>
    
    <div class="pagExin04-contentBk">
        <div class="max_width">
            <div class="pagExin04-sectionBk pagExin04-sectionBk--01">
                <img src="images/psgEx4-15.png" alt="" class="img01">
                <img src="images/psgEx4-30.png" alt="" class="img02">
                <img src="images/psgEx4-33.png" alt="" class="img03">
                <img src="images/psgEx4-34.png" alt="" class="img04">
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--02">
                <h3 class="pagExin04-sectionTit">
                    終身學習​ ∣ Lifelong Learning
                </h3>
                <p class="pt-25 typo-black">
                    鼓勵「終身學習」是LTTC的初衷。 我們陪伴許多朋友走過求學、求職、就業、進修不同人生階段。LTTC的學員和考生來自全國不同城巿，各自有不同的身分 ；從學童、青少年、社會中堅、樂活族，相同的是他們都在這裡找到學習的樂趣和成就。
                    <br>
                    <br>
                    Promoting "lifelong learning" is the original intent of the LTTC. We’ve accompanied many of our learners through different stages in life—from school and job searches to employment and advanced studies. LTTC students and test takers come from all corners of Taiwan, each with a different identity. From schoolchildren and teenagers to adults and active seniors, the one thing they have in common is that they all discover the joy and accomplishment of learning here.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--03">
                <div class="tableBk01">
                    <img src="images/psgEx4-label01--sml.svg" alt="" class="label--sml" width="100%">
                    <img src="images/psgEx4-label01--big.svg" alt="" class="label--big mb-40" width="100%">
                    <img src="images/psgEx4-16.png" alt="" class="mt-40 mb-40" width="100%">
                    <p class="typo-black">
                        LTTC課程學員涵蓋各年齡層，主要招收18歲以上學習者。隨國內學校教育推動英語、第二外語之學習，LTTC近年開辦中學生素養導向課程，擴大了學員的年齡分布。
                        <br>
                        <br>
                        LTTC learners span all age groups, although most are over 18 years old. As Taiwan promotes the learning of English language and a second foreign language, the LTTC has launched competency oriented courses for middle-school students in recent years, which have expanded the age distribution of students.
                    </p>
                </div>
                <div class="tableBk01">
                    <img src="images/psgEx4-label02--sml.svg" alt="" class="label--sml" width="100%">
                    <img src="images/psgEx4-label02--big.svg" alt="" class="label--big mb-40" width="100%">
                    <img src="images/psgEx4-17.png" alt="" class="mt-40 mb-40" width="100%">
                    <p class="typo-black">
                        學員身分以職場人士為主，包含工商服務業、軍公教人員等。
                        <br>
                        <br>
                        The learners are primarily business professionals, along with workers in the military, civil service, and education.
                    </p>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--04">
                <div class="img01">
                    <img src="images/psgEx4-18.png" alt="" class="mb-20" width="100%">
                </div>
                <p class="pt-25 typo-black">
                    以考生年齡分布來看，日本語能力試驗(JLPT)有最年幼的5歲孩童到最年長的96歲長者。
                    <br>
                    此外還有更多人持續設定目標，逐步挑戰。
                    <br>
                    <br>
                    In terms of the age distribution of test takers, the range for the Japanese-Language Proficiency Test (JLPT) is from 5 to 96 years old.
                    In addition, more people continue to set new goals to challenge themselves.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--05">
                <div class="label--sml">
                    <img src="images/psgEx4-label03--sml.svg" alt="" class="label--sml" width="100%">
                </div>
                <div class="label--big">
                    <img src="images/psgEx4-label03--big.svg" alt="" class="" width="100%">
                </div>
                <div class="tableBk01">
                    <p class="typo-black mt-20">
                        擁有4張證書者超過1千人 <br>
                        More than 1,000 people hold 4 certificates. <br><br>
                        擁有3張證書者超過30萬人 <br>
                        More than 300,000 people hold 3 certificates. <br><br>
                        擁有2張證書​者超過21萬人 <br>
                        More than 210,000 people hold 2 certificates. 
                    </p>
                    <img src="images/psgEx4-19.png" alt="" class="mt-40 mb-40" width="100%">
                </div>
                <h3 class="pagExin04-sectionTit">
                    學習導向評量​ ∣ 
                    <!-- <span> -->
                        Learning Oriented Assessment
                    <!-- </span>  -->
                </h3>
                <p class="pt-25 typo-black">
                    過往大眾對於考試的印象，是一張顯示分數的成績單，不管考得好不好，考試就是學習的終點站。​LTTC以鼓勵學習為初衷，將考試評量賦予更具前瞻性價值，希望評量的目的能引導學生或回饋老師，給予建設性的學習或教學建議，有效率地往下一階段目標再前進。
                    因此LTTC在外語訓練或測驗業務，提供個人化的診斷服務。希望落實「學習導向評量」(Learning Oriented Assessment)的理念：「引導」並培養自主學習能力。
                    <br>
                    <br>
                    In the past, the public's impression of exams boiled down to the final score. No matter how well the individual performed, the score was always the end of learning. The LTTC’s intent is to encourage learning for its own sake and to make test evaluation more forward-looking. The goal of assessment is to help students and teachers plan and effectively achieve the next stage of learning. It is for this reason that the center provides individualized diagnostic services in training and testing. The hope is to implement the concept of "Learning Oriented Assessment" to guide and cultivate self-directed learning. 
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--06">
                <h3 class="pagExin04-sectionTit">
                    「GEPT 聽診室」個人化成績回饋​ ∣ 
                    <span>
                        “Dr. GEPT” Offers Individualized Feedback
                    </span> 
                </h3>
                <p class="pt-25 typo-black">
                    「全民英檢」成績公布後，考生可登入「GEPT聽診室」瀏覽自己當次測驗的表現：包括強弱項分析和學習指引，不熟的句型和字彙完整呈現。「GEPT聽診室」甫推出2個月即超過5萬人使用。平均滿意度達9成以上。​
                    <br>
                    <br>
                    After the GEPT results are announced, test takers can log on to the “Dr. GEPT” to view their test performance. Individualized feedback includes analysis of strengths and weaknesses, discussion of unfamiliar sentence structures and vocabulary plus further learning suggestions. The “Dr. GEPT” has so far been used by more than 50,000 people within two months of its launch and achieved an average satisfaction rate of over 90%.  ​ 
                </p>
                <div class="imgBk">
                    <div class="img01">
                        <img src="images/psgEx4-20.png" alt="" class="" width="100%">
                    </div>
                    <div class="img02">
                        <img src="images/psgEx4-21.png" alt="" class="img--sml" width="100%">
                        <img src="images/psgEx4-31.png" alt="" class="img--ipad" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--07">
                <h3 class="pagExin04-sectionTit">
                    GEPT Kids 「小學英檢」個人化成績回饋​ ∣ 
                    <br>
                    “GEPT Kids” Individualized Diagnosis and Analysis 
                </h3>
                <p class="pt-25 typo-black">
                    考後的成績證明，提供18項能力指標的達成率。個人化的具體診斷與分析，能引導學生自主學習。
                    <br>
                    <br>
                    The test certificate provides 18 ability indicators. Individualized diagnosis and analysis can guide students in their self-directed learning.
                </p>
                <div class="imgBk">
                    <div class="img01">
                        <img src="images/psgEx4-03.png" alt="" class="" width="100%">
                    </div>
                    <div class="img02">
                        <img src="images/psgEx4-04.png" alt="" class="" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--08">
                <h3 class="pagExin04-sectionTit">
                    LTTC課程個人化診斷回饋​ ∣ 
                    <br>
                    Individualized Diagnostic Analysis and Feedback in the LTTC Classes
                </h3>
                <p class="pt-25 typo-black">
                    以英語寫作專班為例，個人化診斷項目如下；對症下藥，協助學習者更有效提升寫作能力。
                    <br>
                    <br>
                    In the LTTC writing class for example, the individualized diagnostic analysis and feedback are as follows：
                </p>
                <div class="imgBk">
                    <div class="imgListBk">
                        <p class="list">切題度(relevance& adequacy)</p>
                        <p class="list">段落結構(organization)</p>
                        <p class="list">連貫性(coherence)</p>
                        <p class="list">文法句構(grammatical use)</p>
                        <p class="list">字彙(lexical use)</p>
                    </div>
                    <div class="img01">
                        <img src="images/psgEx4-05.png" alt="" class="" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--09">
                <h3 class="pagExin04-sectionTit">
                    帶動臺灣英語教育​ ∣ 
                    <span>
                        A Driving Force of English Education in Taiwan​ 
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    LTTC研發的「全民英檢」(GEPT) 是國內最早完整提供接收技能（聽、讀）與產出技能（說、寫）的英語評量。
                    相較於早期國內入學考試僅檢測英文讀、寫能力，「全民英檢」鼓勵學習者均衡發展四種英語技能，因此帶動聽、讀、說、寫四項均衡發展的溝通與正向的語言學習風氣。​
                    <br><br>
                    Developed by the LTTC, the GEPT is the first English assessment in Taiwan to cover both receptive skills (listening and reading) and productive skills (speaking and writing). Compared to earlier assessments, which tested only reading and writing skills, the GEPT encourages learners to develop all four modes of communication (listening, reading, speaking and writing) in a balanced manner. This has resulted in a more well-rounded and positive approach to language education.
                </p>
                <div class="imgBk">  
                    <div class="img01 pb-40">
                        <img src="images/psgEx4-22.png" alt="" class="ptb-30" width="100%">
                        <p class="typo-black">
                            英語學習風氣向下扎根，考生有年輕化趨勢。
                            <br><br>
                            English education has firmly taken  root in Taiwan. The starting age of test takers has also dropped.
                        </p>
                    </div>
                    <div class="img02 pb-40">
                        <img src="images/psgEx4-23.png" alt="" class="ptb-30" width="100%">
                        <p class="typo-black">
                            英語說寫能力日受重視，超過8成通過聽讀考生2年內接續考說寫測驗。
                            <br><br>
                            Learners are putting more emphasis on speaking and writing. 80% of learners who have passed the listening & reading parts of the GEPT return within 2 years to complete the speaking & writing parts.
                        </p>
                    </div>
                </div>
                <div class="img03">
                    <img src="images/psgEx4-24.png" alt="" class="ptb-30" width="100%">
                    <p class="typo-black">
                        聽讀能力逐年提升，20年來考生中級聽力平均進步11分、閱讀進步9分、通過率提升超過2成
                        <br><br>
                        Learners’ listening and reading ability has  been improving yearly. Over the past 20 years, test takers have improved an average of 11 points in intermediate listening and 9 points in reading, and the pass rate has increased by more than 20%
                    </p>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--10">
                <!-- <h3 class="label">
                    知識的循環與回饋
                    <br>
                    The Knowledge Feedback Loop 
                </h3> -->
                <div class="label--sml">
                    <img src="images/psgEx4-label04--sml.svg" alt="" class="label--sml" width="100%">
                </div>
                <div class="label--big">
                    <img src="images/psgEx4-label04--big.svg" alt="" class="" width="100%">
                </div>
                <div class="img01">
                    <img src="images/psgEx4-25.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-26.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-27.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-28.png" alt="" class="" width="100%">
                </div>
                <div class="img02">
                    <img src="images/psgEx4-32.png" alt="" class="" width="100%">
                </div>
                <p class="typo-black">
                    LTTC除了陪著社會大眾學習成長，也長期致力於協助提升英語老師識能，定期辦理教學與評量相關課程活動，將累積的評量/教學成果/經驗與大眾分享，​與英語教育工作者一同精進。
                    <br><br>
                    In addition to its support for education of the general public, the LTTC is also committed to improving the skills of English teachers. The center regularly organizes teaching and assessment-related curriculum-based events, working shoulder-to-shoulder with English educators to share accumulated assessments, teaching results and experience.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--11">
                <div class="img01">
                    <img src="images/psgEx4-06.png" alt="" class="" width="">
                    <p class="pt-15 typo-black">
                        十二年國教召集人張武昌教授主講「素養導向評量之特色與示例」
                        <br>
                        The convener of the twelve-year public education, Professor Vincent W. Chang gives a lecture on Competency Oriented Assessment.
                    </p>
                </div>
                <div class="img02">
                    <img src="images/psgEx4-07.png" alt="" class="" width="">
                    <p class="pt-15 typo-black">
                        老師們專注投入工作坊互動
                        <br>
                        Participating teachers focus on workshop interaction.
                    </p>
                </div>
                <div class="img03">
                    <img src="images/psgEx4-08.png" alt="" class="img--sml" width="">
                    <img src="images/psgEx4-09.png" alt="" class="img--big" width="">
                    <p class="pt-15 typo-black">
                        2020創新教學與評量研討會 & LTTC 素養導向評量工作坊
                        <br>
                        2020 Innovative Teaching and Evaluation Seminar & LTTC Competency Oriented Assessment Workshop. 
                    </p>
                </div>
                <h3 class="pagExin04-sectionTit">
                    提升教師素養導向命題識能​ ∣ 
                    <span>
                        Enhancing Teachers' Competency Oriented Test Writing Knowledge 
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    辦理「素養導向英語命題徵選活動」，鼓勵教師提升素養導向命題識能，並希望藉由優秀之命題作為範例，提供實用的資源，有效連結學習、教學與評量。
                    <br><br>
                    The “Competency Oriented English Test Question Competition" was held to encourage teachers to improve their knowledge of Competency Oriented assessment. In addition, the winning test questions served as practical examples for educators, effectively linking learning, teaching and assessment. 
                </p>
            </div>
        </div>
    </div>
    
    <div class="pagExin04-contentBk pagExin04-contentBk--bgYellow">
        <div class="max_width">
            <div class="pagExin04-sectionBk pagExin04-sectionBk--12">
                <h3 class="pagExin04-sectionTit">
                    臺灣英語學習者語料庫 ∣ 
                    <span>
                        Taiwan English Learner Corpus (LTTC-ELC)
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    收錄「全民英檢」(GEPT) 考生作答，建置兩百萬個詞彙的臺灣學習者寫作語料庫，並發展語料庫搜尋軟體，免費提供語料分析。LTTC-ELC語料庫提供老師科學、系統化的工具，分析臺灣學生英語寫作的盲點或困難處，作為教學加強之重點參考。LTTC-ELC語料庫提供三種不同的搜尋功能，包括關鍵字(keyword)、搭配詞(collocation)、連續多字搜尋。
                    <br>
                    • 關鍵字搜尋：可以用來瞭解特定的詞彙使用上常見的錯誤，例如information是不可數名詞，但許多學習者常使用複數informations。
                    <br>
                    • 搭配詞搜尋：可以用來瞭解字和字之間慣常搭配使用的組合，例如contact作為動詞(contact someone)與名詞(keep in contact with someone)的用法容易混淆。
                    <br>
                    • 連續多字搜尋：可以用來瞭解慣用語、成語或片語等學習上的困難，例如In my opinion的正確用法應是後方直接接論點，但結果發現語料庫出現 In my opinion, I think…句型的頻率相當高) 
                    <br><br>
                    The LTTC built a two-million-word Taiwanese English Learner Corpus, based on all test answers from the GEPT, while providing free corpus analysis. The LTTC-ELC corpus provides teachers with scientific and systematic tools to analyze the blind spots and difficulties that Taiwanese students face when writing in English. These tools act as a key reference for teaching enhancement. The LTTC-ELC corpus provides three different search functions: keyword, collocation, and continuous multi-word (Ngram) search. 
                    <br>
                    • Keyword search: This can be used to understand common mistakes in the use of specific vocabulary. For example, “information” is an uncountable noun, but many learners often use the plural “informations”.
                    <br>
                    • Collocation search: This can be used to understand the common combination of words with other words. For example, the usage of “contact” as a verb (“contact someone”) and noun (“keep in contact with someone”) is easily confused. 
                    <br>
                    • Continuous multi-character (Ngram) search: This can be used to understand difficulties with items such as idioms or phrases. For example, the correct usage of “In my opinion” should be to directly introduce the following argument, but it turns out the frequency of sentence structures such as “In my opinion, I think...” is quite high in the corpus.       
                </p>
                <div class="img01">
                    <img src="images/psgEx4-10.png" alt="" class="" width="100%">
                </div>
            </div>
            
        </div>
    </div>

    
    <!-- 首頁底元素 -->
    <div class="pagExin04-contentBk--bgYellow">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.svg" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     