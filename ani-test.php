<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>

<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<!-- 抓banner圖 -->
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image:width" content="1010" />
<meta property="og:image:height" content="1010" />

<?php require('head.php') ?>

<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'flipInX',
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        // nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1024: {
                items: 2
            },
        }
    });
});

// 動畫效果
$(document).ready(function() {
    // loading 動畫效果出現    
    gsap.registerPlugin(ScrollTrigger);

    // ScrollTrigger.saveStyles(".js-loadingCircle01");
    ScrollTrigger.matchMedia({
        
    // desktop
    "(min-width: 1440px)": function() {
        gsap.to(".js-loadingCircle01", {
            scrollTrigger: {
                trigger: ".js-loadingCircle01",
                start: "top center",
                toggleActions: "play pause resume none",
                scrub: true,
                markers: true,
            },
            x: 500,
            duration: 2.5, 
            rotation: 360, 
            //scrollTrigger和repeat一起使用會發生定位問題
            // repeat: 5,
        });
        gsap.to(".js-loadingCircle02", {
            scrollTrigger: {
                trigger: ".js-loadingCircle02",
                start: "top center",
                /**/
                toggleActions: "restart pause resume pause",
                scrub: false,
                markers: true,
            },
            x: 500,
            duration: 3,
            rotation: 360,
            scale: 0.5,
            // repeat: 2,
        });
    },
  
    // mobile
    "(max-width: 768px)": function() {
        gsap.to(".js-loadingCircle01", {
            duration: 3,
            rotation: 360,
            scale: 0.5,
            repeat: 2,
        });
    },
      
    // all 
    "all": function() {
      // ScrollTriggers created here aren't associated with a particular media query,
      // so they persist.
    }
      
  }); 
});

$(window).on('load',function(){

});
</script>
<body>
    <!-- loading動畫 -->
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <img src="images/newink01.png" alt="" class="patLoading-color01 js-loadingCircle01">
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <img src="images/newink02.png" alt="" class="patLoading-color02 js-loadingCircle02">
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <br /> 
    <!-- <picture>
        <source srcset="images/banner01-portrait.png" media="(max-width: 48em)" width="100%" height="auto">
        <source srcset="images/banner01-landscape.png" media="(min-width: 48em)" width="100%" height="auto">
        <img src="images/banner01.png" alt="預設輪播圖" width="100%" height="auto">    
    </picture> -->




    
        

</body>
</html>

     