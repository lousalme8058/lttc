<!DOCTYPE html>	
<head>
<title>LTTC70週年慶網站</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(window).on('load',function(){

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({

    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {          
        gsap.set(".js-pagWishMainDec-Img04", {
            opacity: 0,
            x: 100,
            y: -40,
        });
        gsap.set(".js-pagWishMainDec-Img05", {
            opacity: 0,
            x: -100,
            y: 40,
        });
        gsap.set(".js-pagWishMainDec-Img01", {
            opacity: 0,
            y: 100,
        });
        var tl = gsap.timeline();
        gsap.to(".js-pagWishMainDec-Img04",{
            delay: 1.5,
            duration: 1.2,
            opacity: 1,
            x: 0,
            y: 0,
        });
        gsap.to(".js-pagWishMainDec-Img05",{
            delay: 1.7,
            duration: 1.2,
            opacity: 1,
            x: 0,
            y: 0,
        });
        gsap.to(".js-pagWishMainDec-Img01", {
            scrollTrigger: {
                trigger: ".js-pagWishMainDec-Img01",
                start: "top center+=100",
                // endTrigger: ".indAct-link",
                // end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            y: 0,
            duration: 1.2, 
            opacity: 1,
            delay: 0.5,
        });
        
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagWishBg">

    <?php require('loading.php') ?>
    <?php require('smlNav.php') ?>
    <?php require('headerBgyellow.php') ?>

    <div class="pagWishTitBk">
        <h1>
            各方祝福
            <br>
            Well-wishers
        </h1>  
    </div>

    <div class="pagWishVideoMainBK">
        <div class="pagWishVideoMain">
            <div class="baseVideoBk">
                <iframe width="" height="" src="https://www.youtube.com/embed/qEMZOfU0gPo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <img src="images/wish03.svg" alt="右邊女生" class="pagWishMainDec-Img04 js-pagWishMainDec-Img04">
    <img src="images/wish02.svg" alt="左邊女生" class="pagWishMainDec-Img05 js-pagWishMainDec-Img05">
    <div class="pagWishMainDecBk">
        <div class="pagWishMainDec-Cloud"></div>
        <img src="images/wish01.svg" alt="生日蛋糕+人物" class="pagWishMainDec-Img01 js-pagWishMainDec-Img01">
        <img src="images/indele08.png" alt="黃星" class="pagWishMainDec-Img02">
        <img src="images/indele09.png" alt="藍星" class="pagWishMainDec-Img03">
    </div>
    
    <div class="pagWishVideoListBk">
        <div class="max_width">
            <div class="pagWishVideoList">
                
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/qEMZOfU0gPo" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學校長/LTTC董事長
                                <br>
                                管中閔
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/qEMZOfU0gPo/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/b4whRfQOGyY" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                語言訓練測驗中心(LTTC)執行長
                                <br>
                                沈冬
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/b4whRfQOGyY/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/abBNn9hqrug" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                越南外國語大學校長
                                <br>
                                Do Tuan Minh
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/abBNn9hqrug/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/iGF_onn853Y" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                日本英語檢定協會EIKEN
                                <br>
                                仲村圭太
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/iGF_onn853Y/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/S4_bkfQKIq4" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                韓國高麗大學教授
                                <br>
                                Inn Chull Choi
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/S4_bkfQKIq4/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/2d0fwKm0Lr4" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                上海交通大學教授
                                <br>
                                金艷
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/2d0fwKm0Lr4/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/L-RjiaRFIGs" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國文化協會測評研發中心主任
                                <br>
                                Barry O'Sullivan
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/L-RjiaRFIGs/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Gx5dQDxqWYM" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國貝德福德大學教授
                                <br>
                                Tony Green
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Gx5dQDxqWYM/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/fxoeu1fR6ps" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國蘭卡斯特大學教授
                                <br>
                                Luke Harding
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/fxoeu1fR6ps/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/0cpu2qXPdG4" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                澳洲墨爾本大學部門副主任
                                <br>
                                Jason Fan
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/0cpu2qXPdG4/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Z7PX0IJjPcM" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                            	美國加州大學榮譽教授
                                <br>
                                Lyle Bachman 
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Z7PX0IJjPcM/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/ZIQXoVVM72M" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國劍橋考試院部門總監
                                <br>
                                Nick Saville 
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/ZIQXoVVM72M/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/0d0TD2304Bs" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                日本台灣交流協會代表
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/0d0TD2304Bs/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/u0jczqhHVl8" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國文化協會臺灣處長
                                <br>
                                Ralph Rogers 
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/u0jczqhHVl8/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/pD4ZO8ScYe8" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                日本京都大學iUP專案代表
                                <br>
                                河合淳子 
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/pD4ZO8ScYe8/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/NRsNdxIuJ2c" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                英國南安普敦大學資深講師
                                <br>
                                Robert Baird
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/NRsNdxIuJ2c/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Nl7X5xi1ZG4" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                韓國首爾大學教授
                                <br>
                                Yong-Won Lee
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Nl7X5xi1ZG4/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/sKewpOsOk2U" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國家發展委員會社會發展處處長
                                <br>
                                張富林
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/sKewpOsOk2U/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/-XL9woe9Xac" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                教育部國際與兩岸教育司司長
                                <br>
                                李彥儀
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/-XL9woe9Xac/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Ja0FmYtzcMs" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                外交部外交及國際事務學院副院長
                                <br>
                                高安
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Ja0FmYtzcMs/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/pNx_aZnt6Ys" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                學術交流基金會董事
                                <br>
                                吳靜吉
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/pNx_aZnt6Ys/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/CIZmvBvZ4R0" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學日文系教授
                                <br>
                                朱秋而
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/CIZmvBvZ4R0/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/VRAGpZ_TfaE" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學外文系教授
                                <br>
                                邱錦榮
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/VRAGpZ_TfaE/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/fRkA2LZXP3A" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學外文系教授
                                <br>
                                張淑英
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/fRkA2LZXP3A/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/9ouzpgMUrtw" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國家發展委員會綜合規劃處處長
                                <br>
                                張惠娟
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/9ouzpgMUrtw/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/bjn3o_-qUY4" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學外文系名譽教授
                                <br>
                                林耀福
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/bjn3o_-qUY4/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/gU4hHhnsCZ0" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學外文系教授
                                <br>
                                梁欣榮
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/gU4hHhnsCZ0/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/PnsYnOcvzAs" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                世新大學外文系教授
                                <br>
                                高天恩
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/PnsYnOcvzAs/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/o2ztfaH-j8s" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣師範大學十二年國教英語科召集人
                                <br>
                                張武昌
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/o2ztfaH-j8s/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/tYYMF02vEjE" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                南臺科技大學人文社會學院院長
                                <br>
                                黃大夫
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/tYYMF02vEjE/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/o2WM9c6bDGE" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                文藻外語大學校長
                                <br>
                                陳美華
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/o2WM9c6bDGE/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/SKWVhMY-Wxg" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學國際長
                                <br>
                                袁孝維
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/SKWVhMY-Wxg/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/E3kAJ_kHJkM" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣科技大學大人文社科學院院長
                                <br>
                                李思穎
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/E3kAJ_kHJkM/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/RINMc-Wo2rc" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學寫作教學中心
                                <br>
                                李維晏主任與同仁
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/RINMc-Wo2rc/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/p1Kh-CaEYuA" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學語言所所長
                                <br>
                                謝舒凱
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/p1Kh-CaEYuA/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Z2hVNnT4thY" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                幫你優執行長
                                <br>
                                葉丙成
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Z2hVNnT4thY/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/KNbEyZHBP_Y" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學師培中心主任
                                <br>
                                傅昭銘
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/KNbEyZHBP_Y/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/Rd4zWUXGZDw" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學出版中心主任
                                <br>
                                張俊哲
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/Rd4zWUXGZDw/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/nh96OstNPo8" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立鳳新高中老師
                                <br>
                                邱美江
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/nh96OstNPo8/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/7vt09oLW8CY" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學外文系教授
                                <br>
                                廖咸浩
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/7vt09oLW8CY/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
                <article class="pagWishVideoLinkBk">
                    <a href="https://youtu.be/mTxBQiReQus" class="pagWishVideoLink" target="_blank">
                        <div class="pagWishVideoLink-layer">
                            <p class="pagWishVideoLink-layer--tit">
                                國立臺灣大學語言所教授
                                <br>
                                蘇以文
                            </p>
                        </div>
                        <img src="http://img.youtube.com/vi/mTxBQiReQus/hqdefault.jpg" alt="封面" class="pagWishVideoLink-cover" width="">
                    </a>
                </article>
            </div>
        </div>
    </div>


    <!-- 首頁底元素 -->
     <div class="pagWish--bgYellow">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
        </div>
    </div>
    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
   

</body>
</html>

     