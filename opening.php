<!DOCTYPE html>	
<head>
<title>LTTC70週年慶網站</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<script language="javascript">

// 動畫效果
$(window).on('load',function(){

    gsap.registerPlugin(ScrollTrigger);
    // ScrollTrigger.saveStyles(".indBanner-main");

    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {
 
    },
  
    // mobile
    "(max-width: 768px)": function() {
    
    },
      
    // all 
    "all": function() {
        //banner區
        gsap.set(".js-pagOpSection01-img", {
            opacity: 0,
            x: 100,
        });
        gsap.set(".js-pagOpSection01-chTextArea", {
            opacity: 0,
            x: -100,
        });
        gsap.set(".js-pagOpSection01-enTextArea", {
            opacity: 0,
            x: -100,
        });
        gsap.set(".js-pagOpSection02-img", {
            opacity: 0,
            y: -100,
        });
        gsap.set(".js-pagOpSection02-chTextArea", {
            opacity: 0,
            y: 100,
        });
        gsap.set(".js-pagOpSection02-enTextArea", {
            opacity: 0,
            y: 100,
        });
        var tl = gsap.timeline();
        tl.to(".js-pagOpSection01-img",{
            delay: 1.5,
            duration: 1,
            x: 0,
            opacity: 1,
        })
        tl.to(".js-pagOpSection01-chTextArea", {
            x: 0,
            duration: 1, 
            opacity: 1,
            delay: 0,
        });
        gsap.to(".js-pagOpSection01-enTextArea", {
            scrollTrigger: {
                trigger: ".js-pagOpSection01-enTextArea",
                start: "top center",
                // endTrigger: ".indAct-link",
                // end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            x: 0,
            duration: 1, 
            opacity: 1,
            delay: 0.5,
        });
        gsap.to(".js-pagOpSection02-img", {
            scrollTrigger: {
                trigger: ".js-pagOpSection02-img",
                start: "top center",
                // endTrigger: ".indAct-link",
                // end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            y: 0,
            duration: 1, 
            opacity: 1,
            delay: 0.5,
        });
        gsap.to(".js-pagOpSection02-chTextArea", {
            scrollTrigger: {
                trigger: ".js-pagOpSection02-chTextArea",
                start: "top center+=100",
                // endTrigger: ".indAct-link",
                // end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            y: 0,
            duration: 1.2, 
            opacity: 1,
            delay: 0.5,
        });
        gsap.to(".js-pagOpSection02-enTextArea", {
            scrollTrigger: {
                trigger: ".js-pagOpSection02-enTextArea",
                start: "top center+=120",
                // endTrigger: ".indAct-link",
                // end: "bottom top",
                toggleActions: "play pause none none",
                scrub: false,
                markers: false,
                /*到strat定位只執行一次*/
                once: true,
            },
            y: 0,
            duration: 1.2, 
            opacity: 1,
            delay: 0.5,
        });
    }
  }); 
});

$(window).on('load',function(){

});

</script>

<body class="">
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('headerBgyellow.php') ?>

    <!-- 第一段落 -->
    <div class="pagOpSection01">
        <div class="max_width overflow-hidden">
            <img src="images/pagOpele01.png" alt="開幕茶會形象圖" class="pagOpSection01-img js-pagOpSection01-img mb-20">
            <div class="pagOpSection01-chTextArea js-pagOpSection01-chTextArea plr-20">
                <h5 class="pagOpSection-TextArea-tit pb-20">70週年慶祝茶會</h5>
                <p class="pagOpSection-TextArea-text pb-40">
                    <!-- 活動時間：2021.06.25 10:00-11:20 -->
                    活動時間：待確認
                    <br />
                    <br />
                    活動地點：臺灣大學社科院梁國樹國際會議廳
                    <br />
                    <br />
                    LTTC為慶祝70週年，特規劃慶祝茶會，邀請LTTC的董事長、歷屆執行長、教育與企業界的友人一同慶祝這特別的一刻。在觥籌交錯之際，我們將回顧70年來的點滴及展望未來、發表新書與舉辦LTTC70週年「跨出自信 世界同行」特展開展儀式，歡迎教育、企業與媒體各界先進屆時蒞臨同樂。
                    <br />
                    <br />
                    活動規劃：<br />
                    1. 長官與貴賓致詞<br />
                    2. 觀賞LTTC70週年紀念影片與祝賀影片<br />
                    3. 新書發表<br />
                    4. 開展儀式<br />
                    5. 茶敘與導覽<br />
                </p>
            </div>
            <div class="clear"></div>
            <div class="pagOpSection01-enTextArea js-pagOpSection01-enTextArea plr-20">
                <h5 class="pagOpSection-TextArea-tit pb-20">70th Anniversary Opening Ceremony</h5>
                <p class="pagOpSection-TextArea-text pb-40">
                    <!-- Date and Time：10:00-11:20 AM, June, 25th, 2021 -->
                    Date and Time：To be Confirmed
                    <br />
                    <br />
                    Venue：The Liang Kuo Shu International Conference Hall, College of Social Sciences, National Taiwan University
                    <br />
                    <br />
                    To celebrate its 70th anniversary, the LTTC will hold a celebration party, to which it will invite the chairperson, all its former CEOs, and friends from the education and business fields to gather together and enjoy this wonderful moment. Meanwhile, at this time of retrospect and prospect, the LTTC will also celebrate the start of its 70th Anniversary Exhibition, "A Big Step into the World," as well as a new book launch. We sincerely welcome participation from the sectors of education, business, and mass media.
                    <br />
                    <br />
                    Activities：<br />
                    1. Remarks from the Chairperson of the Board, the CEO, and Distinguished Guests​<br />
                    2. Release of the 70th Anniversary Video and Celebration Videos​<br />
                    3. New Book Launch​<br />
                    4. Celebration of the Start of the 70th Anniversary Exhibition​<br />
                    5. Tea Break and Guided Tours​<br />
                </p>
            </div>
        </div>
    </div>

    <!-- 頁面中間元素 -->
    <div class="pagOpEleBk">
        <img src="images/indele08.png" alt="element" class="pagOpEle-01">
        <img src="images/indele09.png" alt="element" class="pagOpEle-02">
    </div>

    <!-- 第二段落 -->
    <div class="pagOpSection02">
        <div class="max_width">
            <a id="js-pagOpSection02-ahref" class="pagOpSection02-ahref"></a>
            <img src="images/pagOpele02-sml.png" alt="" class="pagOpSection02-img--sml js-pagOpSection02-img">
            <img src="images/pagOpele02-big.png" alt="" class="pagOpSection02-img--big js-pagOpSection02-img plr-40">
            <div class="pagOpSection02-chTextArea js-pagOpSection02-chTextArea plr-20 pt-25">
                <h5 class="pagOpSection-TextArea-tit pb-20">70週年特展</h5>
                <p class="pagOpSection-TextArea-text pb-40">
                    <!-- 活動時間：2021.06.25-12.25  08:00-17:00 -->
                    活動時間：待確認
                    <br />
                    <br />
                    活動地點：LTTC 1－4樓公共空間
                    <br />
                    <br />
                    70年來，LTTC陪伴著國人在外語學習路上成長蛻變。70年來，LTTC從代工走向自創品牌、從臺灣走向國際、從單一語言服務走向多元語言服務、從傳統走向
                    數位，自始至終不斷前進。LTTC 70週年特展記錄了我們這段時間的成長軌跡與點點滴滴，本特展除了在LTTC大樓內的公共空間實體展出外，亦同步舉辦線上
                    特展，與世界各地的友人們分享LTTC的發展歷程。 世界同行」特展開展儀式，歡迎教育、企業與媒體各界先進屆時蒞臨同樂。
                </p>
            </div>
            <div class="pagOpSection02-enTextArea js-pagOpSection02-enTextArea plr-20 pt-25">
                <h5 class="pagOpSection-TextArea-tit pb-20">70th Anniversary Exhibition Gallery</h5>
                <p class="pagOpSection-TextArea-text pb-40">
                    <!-- Date and Time：08:00 AM-05:00 PM, June, 25th-December, 25th, 2021 -->
                    Date and Time：To be Confirmed
                    <br />
                    <br />
                    Venue：Public space at the LTTC
                    <br />
                    <br />
                    For the past seven decades, the LTTC has been a constant companion of foreign language learners in Taiwan. To accommodate their changing needs, the LTTC has been adjusting the roles it plays and the educational services it provides. The LTTC has shown steadfast dedication to innovation and transformation: from a test administrator to a test developer, from English-based to multi-lingual learning and testing, from traditional to digital platforms, and from Taiwan to the global stage. The LTTC 70th Anniversary Exhibition Gallery traces our growth and development over these seven decades and presents touching stories behind the scenes. In addition to displays in the public space at the LTTC, there will also be online exhibitions to share our stories and experiences with friends around the world.
                </p>
            </div>
        </div>
    </div>

    <!-- 首頁底元素 -->
    <div class="indBottomEleBk">
        <img src="images/indele10.png" alt="element" class="indBottomEle01">
        <img src="images/indbanner04.png" alt="element" class="indBottomEle02">
        <img src="images/indele08.png" alt="element" class="indBottomEle03">
        <img src="images/indele09.png" alt="element" class="indBottomEle04">
    </div>

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     