<!-- 小視口全域導覽列 -->
<header class="patSmlHeader">
	<a href="index.php" class="patSmlHeader-logo" title="回到「語言訓練測驗中心」網站">
		<img src="images/logo.svg" alt="語言訓練測驗中心LOGO" class="">
	</a>
	<div class="patSmlHeader-navbt jsNavSml-bt" tabindex="-1" title="打開網站導覽列">
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon01"></div>
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon02"></div>
	</div>
</header>


<!-- 小視口全域導覽列開合區 -->
<div class="patSmlNav jsNavSmall">
	<!-- 全域導覽列連結 -->
	<ul class="patLevelArea jsAccording">
		<a href="exhibits.php" class="patLevelArea-firstLink jsAccording-firstLik" title="e-Exhibits tabindex="-1" data-i18n="common_about_us">
			線上特展 <br />
			e-Exhibits
		</a>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="javascript:void(0);" class="patLevelArea-firstLink jsAccording-firstLik" title="重點活動" tabindex="-1" data-i18n="common_feature">
			重點活動 <br />
			Events
		</a>
		<!-- 第二層 -->
		<ul class="patLevelArea-secondLinkArea jsAccording-secondArea">
			<a href="opening.php" class="patLevelArea-secondLinkArea--link" title="慶祝茶會" tabindex="-1" data-i18n="">
				慶祝茶會 <br />
				Opening Ceremony
			</a>
			<a href="opening.php#js-pagOpSection02-ahref" class="patLevelArea-secondLinkArea--link" title="現場展覽" tabindex="-1" data-i18n="">
				現場展覽 <br />
				Exhibition Gallery
			</a>
		</ul>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="wish.php" class="patLevelArea-firstLink jsAccording-firstLik" title="各方祝福" tabindex="-1" data-i18n="common_news">
			各方祝福 <br />
			Well-wishers
		</a>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="https://www.lttc.ntu.edu.tw/en-trends" target="_blank"  class="patLevelArea-firstLink jsAccording-firstLik" title="學外語抽好禮" tabindex="-1" data-i18n="common_news">
			學外語抽好禮 <br />
			Prize Draw
		</a>
		<div class="clear"></div>
	</ul>
</div>

