<!-- 大事記區 -->
<div class="indMemoBk">
    <!-- 遮罩動畫 -->
    <div class="indMemoBk--layer">
        <img src="images/indele08.svg" alt="hand" class="indMemoBk--hand">
        <p class="indMemoBk--word">
            請按住滑鼠拖曳或手指左右移動
            <br />
            點擊視窗離開提示畫面
        </p>
    </div>
    <!-- tit -->
    <div class="indMemo-titBk">
        <div class="indMemo-tit">
            <h5>大事記表</h5>
            <h6>Milestones</h6>
        </div>
    </div>

    <!-- 元素 -->
    <div class="indMemo-eleBk">
        <img src="images/indele05.png" alt="" class="indMemo-eleBk--01">
        <img src="images/indele06.png" alt="" class="indMemo-eleBk--02">
        <img src="images/indele07.png" alt="" class="indMemo-eleBk--03">
    </div>

    <!-- 大事記區 -->
    <div class="indMemo">
        <div class="indMemo-barTitBk">
            <p class="indMemo-barTit mb-5">臺灣外語教育大事記</p>
            <p class="indMemo-barTit mb-30">Milestones for Foreign Language <br />Education in Taiwan</p>
            <p class="indMemo-barTit mb-5">LTTC 大事記</p>
            <p class="indMemo-barTit">Milestones in LTTC History</p>
        </div>
        <img src="images/indele12.svg" alt="bar" class="indMemo-bar">

        <!-- 01 -->
        <p id="back01" class="indMemo-year indMemo-year01">1951</p>
        <div class="indMemo-tai indMemo-tai01">
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    臺美在教育、文化、經濟及軍事援助等交流密切，美援開始。
                    <span class="indMemo-content--enText">
                    The US begins an extensive aid program in Taiwan to provide military and economic assistance, including funding for culture and education.
                    </span>
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc01">
            <article class="indMemo-content">
                <img src="images/indimg01.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    於美援計畫期間成立，初名為「英語訓練中心」。
                    <span class="indMemo-content--enText">
                        Under the US aid program, the Language Training & Testing Center (LTTC) is founded under the name “English Training Center.”
                    </span>
                </p>     
            </article>
        </div>

        <!-- 02 -->
        <p id="" class="indMemo-year indMemo-year02">1965</p>
        <div class="indMemo-lttc indMemo-lttc02">
            <article class="indMemo-content">
                <img src="images/indimg02.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    改組為「語言中心」，同年建立國內首座視聽語言教室，並推出國內第一套「外語能力測驗(FLPT)」。
                    <span class="indMemo-content--enText">
                        The English Training Center is renamed the Language Center, coinciding with the establishment of Taiwan’s first language lab. The Foreign Language Proficiency Test (FLPT) is launched the same year under the administration of the Language Center.
                    </span>
                    <a href="#1965" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 03 -->
        <p id="back1966" class="indMemo-year indMemo-year03">1966</p>
        <div class="indMemo-lttc indMemo-lttc03">
            <article class="indMemo-content">
                <img src="images/indimg03.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「托福測驗(TOEFL)」。
                    <span class="indMemo-content--enText">
                    The Language Center starts to administer the Test of English as a Foreign Language (TOEFL) on behalf of the US Educational Testing Service (ETS).                        </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 04 -->
        <p id="back1968" class="indMemo-year indMemo-year04">1968</p>
        <div class="indMemo-tai indMemo-tai04">
            <article class="indMemo-content">
                <!-- <img src="images/indimg03.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    國民義務教育延長為9年。
                    <span class="indMemo-content--enText">
                    Taiwan extends compulsory education to nine years , marking a new chapter in the nation’s development.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 05 -->
        <p id="back1970" class="indMemo-year indMemo-year05">1970</p>
        <div class="indMemo-lttc indMemo-lttc05">
            <article class="indMemo-content">
                <img src="images/indimg04.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    觀光局採用FLPT作為導遊人員的檢定標準。
                    <span class="indMemo-content--enText">
                    The Tourism Bureau adopts the FLPT as a requirement for tour guide certification.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 06 -->
        <p id="back1976" class="indMemo-year indMemo-year06">1976</p>
        <div class="indMemo-lttc indMemo-lttc06">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部採認FLPT作為擬赴日歐留學之語言能力標準。
                    <span class="indMemo-content--enText">
                    Taiwan’s Ministry of Education (MOE) adopts the FLPT for assessing the language abilities of applicants wishing to pursue academic studies in Japan and Europe.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 07 -->
        <p id="back1979" class="indMemo-year indMemo-year07">1979</p>
        <div class="indMemo-tai indMemo-tai07">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    臺美斷交。
                    <span class="indMemo-content--enText">
                        The US government breaks diplomatic ties with Taiwan.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc07">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    改名為語言訓練測驗中心(The Language Training and Testing Center)，簡稱LTTC。
                    <span class="indMemo-content--enText">
                        The Language Center is officially renamed “The Language Training & Testing Center,” or the LTTC.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 08 -->
        <p id="back1983" class="indMemo-year indMemo-year08">1983</p>
        <div class="indMemo-tai indMemo-tai08">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部將高級中學課程標準的第二外國語納入選修課程。
                    <span class="indMemo-content--enText">
                        The MOE mandates the inclusion of 2nd foreign language elective courses for senior high school students.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc08">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「多益測驗(TOEIC)」。                        
                    <span class="indMemo-content--enText">
                        The LTTC begins to administer the Test of English for International Communication (TOEIC) on behalf of the ETS.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 09 -->
        <p id="back1984" class="indMemo-year indMemo-year09">1984</p>
        <div class="indMemo-lttc indMemo-lttc09">
            <article class="indMemo-content">
                <img src="images/indimg05.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    承辦教育部全國大專英、日語演講決賽。                        
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the MOE to organize collegiate-level speech contests in English and Japanese.
                    </span>
                    <a href="#1984" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 010 -->
        <p id="back1985" class="indMemo-year indMemo-year010">1985</p>
        <div class="indMemo-lttc indMemo-lttc010">
            <article class="indMemo-content">
                <img src="images/indimg06.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    遷入臺大校總區語文大樓(中心現址)。                       
                    <span class="indMemo-content--enText">
                        The LTTC moves into National Taiwan University’s Language Center Building, where it continues to operate today.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 011 -->
        <p id="back1986" class="indMemo-year indMemo-year011">1986</p>
        <div class="indMemo-lttc indMemo-lttc011">
            <article class="indMemo-content">
                <img src="images/indimg07.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    正式登記為文教財團法人。                   
                    <span class="indMemo-content--enText">
                        The LTTC officially registers to become an educational foundation.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 012 -->
        <p id="back1991" class="indMemo-year indMemo-year012">1991</p>
        <div class="indMemo-lttc indMemo-lttc012">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「日本語能力試驗(JLPT)」。                 
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Japanese-Language Proficiency Test (JLPT).
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 013 -->
        <p id="back1996" class="indMemo-year indMemo-year013">1996</p>
        <div class="indMemo-tai indMemo-tai013">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部選定3所高中辦理為期3年之「推動高級中學選修第二外語課程實驗計畫」。                 
                    <span class="indMemo-content--enText">
                        The MOE selects three pilot schools to provide 2nd foreign language instruction at senior high schools.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc013">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    設立電腦測驗中心(Prometric Testing Center)。               
                    <span class="indMemo-content--enText">
                        The LTTC establishes the Prometric Testing Center to administer computer-based tests.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 014 -->
        <p id="back1997" class="indMemo-year indMemo-year014">1997</p>
        <div class="indMemo-lttc indMemo-lttc014">
            <article class="indMemo-content">
                <img src="images/indimg07-1.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    推出「大學校院英語能力測驗(CSEPT)」。           
                    <span class="indMemo-content--enText">
                    The LTTC launches the College Student English Proficiency Test (CSEPT).
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 015 -->
        <p id="back1998" class="indMemo-year indMemo-year015">1998</p>
        <div class="indMemo-tai indMemo-tai015">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部正式宣示推動「終身學習」之政策，「推廣全民外語學習」為行動方案之一。    
                    <span class="indMemo-content--enText">
                        The MOE officially introduces a new policy to promote the concept of “life-long learning,” which includes the promotion of foreign language learning.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc015">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理GRE測驗。  
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Graduate Record Examinations (GRE) General Test.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 016 -->
        <p id="back1998" class="indMemo-year indMemo-year016">1999</p>
        <div class="indMemo-tai indMemo-tai016">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部於全國實施「推動高級中學第二外語教育5年計畫」。    
                    <span class="indMemo-content--enText">
                        The MOE launches a five-year initiative to establish 2nd foreign language courses at senior high schools across the entire nation.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc016">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部為落實終身學習的理念，特補助全民英檢(GEPT)研發計畫。
                    <span class="indMemo-content--enText">
                        To further promote “life-long learning,” the MOE provides a funding grant to the LTTC to begin development of the General English Proficiency Test (GEPT).
                    </span>
                    <a href="#1999" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 017 -->
        <p id="back2000" class="indMemo-year indMemo-year017">2000</p>
        <div class="indMemo-lttc indMemo-lttc017">
            <article class="indMemo-content">
                <img src="images/indimg08.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                推出「全民英檢(GEPT)」，首度辦理中級測驗。
                    <span class="indMemo-content--enText">
                        The Intermediate-level GEPT is launched.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg08.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    出版日語系列教材《日本語GoGoGo》。
                    <span class="indMemo-content--enText">
                    The LTTC publishes a 4-level Japanese coursebook entitled <i>日本語 GoGoGo</i>.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 018 -->
        <p id="back2001" class="indMemo-year indMemo-year018">2001</p>
        <div class="indMemo-tai indMemo-tai018">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部推動九年一貫課程教育改革。
                    <span class="indMemo-content--enText">
                        The MOE introduces major revisions to its nine-year compulsory education curriculum.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    英語教育全面調整至國小階段，開始全國性實施國小五、六年級英語教育。
                    <span class="indMemo-content--enText">
                    Compulsory English education is implemented in elementary schools, starting in 5th and 6th grades.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc018">
            <article class="indMemo-content">
                <img src="images/indimg09.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    英國雷丁大學Cyril Weir教授獲邀加入GEPT研究委員會。
                    <span class="indMemo-content--enText">
                        Prof. Cyril Weir of the University of Reading joins the GEPT research committee.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部委託辦理「技專校院種子教師研習」及「技專校院學生英語能力檢測及定期能力追蹤計畫」。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the MoE to work with technical and vocational schools to develop English assessment tests for students and training programs for English teachers.
                    </span>
                    <a href="#2001" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 019 -->
        <p id="back2002" class="indMemo-year indMemo-year019">2002</p>
        <div class="indMemo-tai indMemo-tai019">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    臺灣加入世界貿易組織。
                    <span class="indMemo-content--enText">
                        Taiwan joins the World Trade Organization (WTO).
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    行政局推動「挑戰2008：國家發展重點計畫」，首次提出「營造英語生活環境建設計畫」。
                    <span class="indMemo-content--enText">
                        The Executive Yuan announces the <i>Challenge 2008: National Development Plan</i>, which includes plans to create “an English speaking environment” across the nation.
                    </span>
                    <a href="#2002" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc019">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    行政院人事行政局採認FLPT作為「公務人員出國專題研究」及「中高階公務人員短期密集英語訓練班」之甄選標準。
                    <span class="indMemo-content--enText">
                        The Personnel Administration Office of the Executive Yuan adopts the FLPT as screening criteria for civil servants seeking to conduct research overseas or attend short-term English training courses.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「日本留學試驗(EJU)」。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Examination for Japanese University Admission for International Students (EJU).
                    </span>
                    <!-- <a href="#2002" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「德福考試(TestDaF)」。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Test Deutsch als Fremdsprache (TestDaF).
                    </span>
                    <!-- <a href="#2002" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 020 -->
        <p id="back2003" class="indMemo-year indMemo-year020">2003</p>
        <div class="indMemo-lttc indMemo-lttc020">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    設置語言學研究生獎學金。
                    <span class="indMemo-content--enText">
                        The LTTC establishes a scholarship program for postgraduate students in linguistics.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 021 -->
        <p id="back2004" class="indMemo-year indMemo-year021">2004</p>
        <div class="indMemo-lttc indMemo-lttc021">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    GEPT累計考生超過百萬人次。
                    <span class="indMemo-content--enText">
                        The total number of GEPT test takers exceeds one million.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    FLPT、GEPT獲行政院主計處採用為公務人員升等加分之語言認證測驗之一。
                    <span class="indMemo-content--enText">
                        The FLPT and GEPT are officially adopted by the Executive Yuan as measures of language proficiency for civil servants seeking promotion.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「劍橋英語認證(Cambridge Assessment English)」。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer Cambridge Assessment English exams.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 022 -->
        <p id="back2005" class="indMemo-year indMemo-year022">2005</p>
        <div class="indMemo-tai indMemo-tai022">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    政府將英語教育延伸至國小三年級開始辦理。
                    <span class="indMemo-content--enText">
                        English language education is officially expanded to begin at 3rd grade at elementary schools across the nation.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部發布「推動英語能力檢定測驗處理原則」。
                    <span class="indMemo-content--enText">
                        The MOE issues official guidelines on the use of English examinations for certification purposes.                        
                    </span>
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc022">
            <article class="indMemo-content">
                <img src="images/indimg10.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    進行GEPT與CEFR 級數對照研究。
                    <span class="indMemo-content--enText">
                        The first comparison study of the GEPT and the Common European Framework of Reference is conducted.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 023 -->
        <p id="back2006" class="indMemo-year indMemo-year023">2006</p>
        <div class="indMemo-lttc indMemo-lttc023">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「日本導遊測驗」。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer Japan’s Licensed Guide Interpreter Test in Taiwan.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 024 -->
        <p id="back2007" class="indMemo-year indMemo-year024">2007</p>
        <div class="indMemo-lttc indMemo-lttc024">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    GEPT首次於海外(越南)舉辦。
                    <span class="indMemo-content--enText">
                        The GEPT is administered in Vietnam, marking the first time the test is administered overseas.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    與國立臺灣大學語言學研究所合作建置LTTC英語學習者語料庫 (LTTC–ELC)。
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with the Graduate Institute of Linguistics at National Taiwan University to create the LTTC English Learner Corpus and related online learning tools.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    中研院開始委託辦理學術英語寫作課程班。
                    <span class="indMemo-content--enText">
                        The LTTC begins holding English academic writing courses at Academia Sinica, the nation’s top research institution.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 025 -->
        <p id="back2008" class="indMemo-year indMemo-year025">2008</p>
        <div class="indMemo-tai indMemo-tai025">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    行政院續推動「營造國際生活環境建設計畫 」。
                    <span class="indMemo-content--enText">
                        The Executive Yuan continues with official plans “to create an environment friendly to the international community.”
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    成立高級中學第二外語教育學科中心。
                    <span class="indMemo-content--enText">
                        The Foreign Language Education Center for Senior High Schools is established.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc025">
            <article class="indMemo-content">
                <img src="images/indimg11.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    英國蘭卡斯特大學 Charles Alderson教授、美國加州大學洛杉磯分校 Lyle Bachman教授、美國加州州立大學洛杉磯分校Antony Kunnan教授，與澳洲墨爾本大學Tim McNamara教授接受邀請擔任顧問。
                    <span class="indMemo-content--enText">
                        Prof. Charles Alderson from Lancaster Univ., Prof. Lyle Bachman and Prof. Antony Kunnan from UCLA, and Prof. Tim McNamara from the Univ. of Melbourne join the LTTC as consultants for language test development.
                    </span>
                    <a href="#2008" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 026 -->
        <p id="back2009" class="indMemo-year indMemo-year026">2009</p>
        <div class="indMemo-tai indMemo-tai026">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    行政院提出「愛台12建設」總體計畫，其中推動「提升國人英語力建設計畫」。
                    <span class="indMemo-content--enText">
                        The Executive Yuan begins a new major public infrastructure initiative, which includes programs to improve the English proficiency of all citizens.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc026">
            <article class="indMemo-content">
                <img src="images/indimg12.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    開始定期舉辦「LTTC國際學術研討會」。
                    <span class="indMemo-content--enText">
                        The LTTC hosts its first major international academic conference. The LTTC now regularly holds conferences in the fields of language teaching and testing.
                    </span>
                    <!-- <a href="#2008" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg12.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    設置GEPT優秀作文獎。
                    <span class="indMemo-content--enText">
                        The LTTC begins an award program for outstanding GEPT Essays.
                    </span>
                    <a href="#2009" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 027 -->
        <p id="back2010" class="indMemo-year indMemo-year027">2010</p>
        <div class="indMemo-lttc indMemo-lttc027">
            <article class="indMemo-content">
                <img src="images/indimg13.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    GEPT成功爭取國際採認，共獲27國、97校接受作為審核臺灣學生英語能力參考依據。
                    <span class="indMemo-content--enText">
                        The GEPT is internationally recognized: 97 tertiary institutes across 27 countries adopt it as a reliable means of measuring Taiwanese students’ English language ability.
                    </span>
                    <a href="#2010" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 028 -->
        <p id="back2011" class="indMemo-year indMemo-year028">2011</p>
        <div class="indMemo-lttc indMemo-lttc028">
            <article class="indMemo-content">
                <img src="images/indimg14.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    推出「第二外語能力測驗(SFLPT)」。
                    <span class="indMemo-content--enText">
                        The LTTC launches the Second Foreign Language Proficiency Test (SFLPT).
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 029 -->
        <p id="back2012" class="indMemo-year indMemo-year029">2012</p>
        <div class="indMemo-lttc indMemo-lttc029">
            <article class="indMemo-content">
                <img src="images/indimg15.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    第一款APP《全民英檢通》上市。
                    <span class="indMemo-content--enText">
                        The LTTC launches the first GEPT app.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    取得勞委會職訓局「訓練品質評核系統TTQS」(Taiwan TrainQuali System) 評核證書。
                    <span class="indMemo-content--enText">
                        The LTTC obtains certification under the Bureau of Employment and Vocational Training’s Taiwan TrainQuali System.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 030 -->
        <p id="back2013" class="indMemo-year indMemo-year030">2013</p>
        <div class="indMemo-lttc indMemo-lttc030">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    測驗業務行政作業系統通過ISO9001品管認證。
                    <span class="indMemo-content--enText">
                        The LTTC’s Administrative Operating System gains ISO9001 quality management certification.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    出版《語言之道》期刊。
                    <span class="indMemo-content--enText">
                        The LTTC publishes the first issue of the journal <i>The Way of Language</i>.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 031 -->
        <p id="back2014" class="indMemo-year indMemo-year031">2014</p>
        <div class="indMemo-tai indMemo-tai031">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部發布「12年國民基本教育課程綱要總綱」。
                    <span class="indMemo-content--enText">
                        The MOE promulgates the general curricular framework for Twelve-Year Compulsory Education.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc031">
            <article class="indMemo-content">
                <img src="images/indimg16.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    為越南外國語大學University of Language and International Studies （ULIS）辦理英語評量研發工作坊。
                    <span class="indMemo-content--enText">
                        The LTTC provides workshops on the development of English assessment for the University of Language and International Studies (ULIS), Vietnam.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    呼應教育部政策，啟動第二外語向下扎根計畫，與臺北市多個國小合作，辦理兩期歐日語實驗教學課程。
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with elementary schools in Taipei on piloting courses in Japanese and European languages for kids.
                    </span>
                    <a href="#2014" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 032 -->
        <p id="back2015" class="indMemo-year indMemo-year032">2015</p>
        <div class="indMemo-tai indMemo-tai032">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    大學入學考試及國中會考皆正式採計英語聽力成績。
                    <span class="indMemo-content--enText">
                        English listening assessment is included in both college entrance exams and the Comprehensive Assessment Program for Junior High School Students.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc032">
            <article class="indMemo-content">
                <img src="images/indimg17.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    推出「小學英檢（GEPT Kids）」，提供18項的個人化具體診斷與分析。
                    <span class="indMemo-content--enText">
                        GEPT Kids is launched, featuring 18 items of individualized diagnostic feedback.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    辦理「偏鄉國中小學英語教學活動補助計畫」。
                    <span class="indMemo-content--enText">
                        The LTTC begins to fund English teaching activities for elementary and middle schools in remote areas.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 033 -->
        <p id="back2016" class="indMemo-year indMemo-year033">2016</p>
        <div class="indMemo-lttc indMemo-lttc033">
            <article class="indMemo-content">
                <img src="images/indimg18.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    與逢甲大學合作，提供FLPT及SFLPT相關資源，並協助訂定第二外語課程評量標準。
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with Feng Chia University on providing FLPT- and SFLPT-related resources and establishing the evaluation standards for foreign language courses.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    接受委託辦理「韓國語文能力測驗」(TOPIK)。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Test of Proficiency in Korean (TOPIK).
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 034 -->
        <p id="back2017" class="indMemo-year indMemo-year034">2017</p>
        <div class="indMemo-tai indMemo-tai034">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    行政院再次啟動「推動英語為官方語」之討論。
                    <span class="indMemo-content--enText">
                        The Executive Yuan re-proposes the idea of stipulating English as an official language in Taiwan.
                    </span>
                    <a href="#2017-1" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc034">
            <article class="indMemo-content">
                <img src="images/indimg18-1.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    主辦亞洲語言測評研討會AALA & AFELTA，與會人數近300人。
                    <span class="indMemo-content--enText">
                        The LTTC hosts conferences for the Asian Association for Language Assessment and the Academic Forum on English Language Testing in Asia.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    越南教育部委託辦理「大規模英語測驗實務工作坊–以全民英檢為例」。
                    <span class="indMemo-content--enText">
                        The LTTC accepts a commission from the MoE in Vietnam to conduct workshops on the logistics of administering large-scale English tests—using the GEPT as an example.
                    </span>
                    <!-- <a href="#2017-2" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 035 -->
        <p id="back2018" class="indMemo-year indMemo-year035">2018</p>
        <div class="indMemo-tai indMemo-tai035">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    由國家發展委員會提出雙語國家政策發展藍圖，期以2030年為目標，打造臺灣成為雙語國家。
                    <span class="indMemo-content--enText">
                        The National Development Council promulgates the policy of transforming Taiwan into a bilingual country by 2030.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc035">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    設置LTTC語言教學實踐與研究計畫補助專案。
                    <span class="indMemo-content--enText">
                        The LTTC establishes the LTTC Teaching and Research Grant program.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>

        <!-- 036 -->
        <p id="back2019" class="indMemo-year indMemo-year036">2019</p>
        <div class="indMemo-tai indMemo-tai036">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    正式實施108素養導向新課綱。
                    <span class="indMemo-content--enText">
                        The Taiwanese government officially implements the 2019 competency-driven curriculum.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc036">
            <article class="indMemo-content">
                <img src="images/indimg21.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    LTTC主編之《亞洲英語能力測驗：在地化與全球化融合之新模式》專書出版。
                    <span class="indMemo-content--enText">
                        A special volume entitled <i>English Language Proficiency Testing: A New Paradigm Bridging Global and Local Contexts</i> is published.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    臺北市教育局委託研發國小一、二年級雙語生活課程的聽、說評量。
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the Taipei City Government Department of Education to develop listening and speaking assessment tools adopting a CLIL approach for 1st- and 2nd-grade life science courses.
                    </span>
                    <a href="#2019" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 037 -->
        <p id="back2020" class="indMemo-year indMemo-year037">2020</p>
        <div class="indMemo-tai indMemo-tai037">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    教育部發布「中小學國際教育白皮書2.0」，十三項行動方案中包含雙語課程精進計畫。
                    <span class="indMemo-content--enText">
                        The MOE announces its “White Paper on International Education for Primary and Secondary Schools 2.0” with 13 actions plans including the promotion of bilingual education.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc037">
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    新冠肺炎防疫有成，臺灣獨步全球辦理韓語測驗(TOPIK)。
                    <span class="indMemo-content--enText">
                        Thanks to Taiwan’s successful prevention and control of COVID-19, the LTTC becomes the only institute to administer TOPIK during this period.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    GEPT二十周年，考生突破800萬人次。
                    <span class="indMemo-content--enText">
                        The LTTC celebrates the GEPT’s 20th Anniversary. The number of test takers exceeds eight million.
                    </span>
                    <a href="#2020" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

        <!-- 038 -->
        <p id="back2021" class="indMemo-year indMemo-year038">2021</p>
        <div class="indMemo-tai indMemo-tai038">
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    臺北市宣示開啟臺灣IB國際文憑公校元年，擇定一間公立高中推動全臺首所「公立國際文憑(簡稱IB)學校」。
                    <span class="indMemo-content--enText">
                        The Taipei City Government selects one senior high school to pilot the International Baccalaureate (IB) program.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc038">
            <article class="indMemo-content">
                <img src="images/indimg23.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    反映新課綱核心素養，GEPT調整聽讀測驗題型，並基於「學習導向評量」理念推出「GEPT聽診室」，提供考後個人化具體回饋。
                    <span class="indMemo-content--enText">
                        In response to the competency-driven curriculum, the LTTC adjusts the   assessment design of the GEPT’s listening and reading components. Meanwhile, The LTTC also launches the GEPT Diagnosis Room initiative to offer individualized feedback to test takers.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>      
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    LTTC成立七十週年、法人成立三十五年。
                    <span class="indMemo-content--enText">
                        The LTTC celebrates its 70th anniversary and its 35th anniversary as a registered educational foundation.
                    </span>
                    <a href="#2021" class="indMemo-content--moreBt">more...</a>
                </p>      
            </article>
        </div>

    </div>


    <!-- light box -->
    <!-- 1965 -->
    <div class="modLightbox" id="1965">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1965 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            改組為「語言中心」，同年建立國內首座視聽語言教室，並推出國內第一套「外語能力測驗(FLPT)」。
                            <span class="indMemo-content--enText">
                            The English Training Center is renamed the Language Center, coinciding with the establishment of Taiwan’s first language lab. The Foreign Language Proficiency Test (FLPT) is launched the same year under the administration of the Language Center.
                            </span>
                        </p>    
                    </article>  
                    <img src="images/indimg24.png" alt="" class="indMemo-content--img">
                </article>
            </div>
        </div>
    </div>

    <!-- 1984 -->
    <div class="modLightbox" id="1984">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1984 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            承辦教育部全國大專英、日語演講決賽。
                            <span class="indMemo-content--enText">
                                The LTTC launches English, Japanese, and French language courses tailored for pilots and flight attendants at China Airlines.
                            </span>
                        </p>    
                    </article>  
                    <img src="images/indimg25.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            為中華航空公司空服員、機師開辦英、日、法語特別班。
                            <span class="indMemo-content--enText">
                            The LTTC launches English, Japanese, and French language courses tailored for pilots and flight attendants at China Airlines.
                            </span>
                        </p>    
                    </article>  
                </article>
            </div>
        </div>
    </div>

    <!-- 1999 -->
    <div class="modLightbox" id="1999">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1999 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            教育部為落實終身學習的理念，特補助全民英檢(GEPT)研發計畫。
                            <span class="indMemo-content--enText">
                                To further promote “life-long learning,” the MOE provides a funding grant to the LTTC to begin development of the General English Proficiency Test (GEPT).
                            </span>
                        </p>    
                    </article>  
                    <img src="images/indimg26.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            教育部委託辦理國小英語教師能力檢核測驗。
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MOE to develop and administer a special test for assessing the language proficiency of English teachers at elementary schools.
                            </span>
                        </p>    
                    </article>  
                </article>
            </div>
        </div>
    </div>

    <!-- 2001 -->
    <div class="modLightbox" id="2001">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2001 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            英國雷丁大學Cyril Weir教授獲邀加入GEPT研究委員會。
                            <span class="indMemo-content--enText">
                                Prof. Cyril Weir of the University of Reading joins the GEPT research committee.
                            </span>
                        </p>    
                    </article>  
                    <img src="images/indimg28.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            教育部委託辦理「技專校院種子教師研習」及「技專校院學生英語能力檢測及定期能力追蹤計畫」。
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the MoE to work with technical and vocational schools to develop English assessment tests for students and training programs for English teachers.
                            </span>
                        </p>    
                    </article>  
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            設置論文獎。
                            <span class="indMemo-content--enText">
                            The LTTC begins an award program for outstanding thesis and dissertation work in language assessment and teaching.
                            </span>
                        </p>    
                    </article>  
                </article>
            </div>
        </div>
    </div>

    <!-- 2002 -->
    <div class="modLightbox" id="2002">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2002 臺灣外語教育大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                    <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                    <span class="indMemo-content--label">➤</span>
                    <p class="indMemo-content--text">
                        臺灣加入世界貿易組織。
                        <span class="indMemo-content--enText">
                            Taiwan joins the World Trade Organization (WTO).
                        </span>
                        <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                    </p>      
                </article>
                <article class="indMemo-content">
                    <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                    <span class="indMemo-content--label">➤</span>
                    <p class="indMemo-content--text">
                        行政院推動「挑戰2008：國家發展重點計畫」，首次提出「營造英語生活環境建設計畫」。
                        <span class="indMemo-content--enText">
                            The Executive Yuan announces the <i>Challenge 2008: National Development Plan</i>, which includes plans to create “an English speaking environment” across the nation.
                        </span>
                        <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                    </p>      
                </article>
                <article class="indMemo-content">
                    <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                    <span class="indMemo-content--label">➤</span>
                    <p class="indMemo-content--text">
                        行政院首提「推動英語為官方語」之討論。
                        <span class="indMemo-content--enText">
                            The Executive Yuan proposes to make English one of Taiwan’s official languages.
                        </span>
                        <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                    </p>      
                </article>
            </div>
        </div>
    </div>

    <!-- 2005 -->
    <div class="modLightbox" id="2005">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2005 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            政府再將英語教育延伸至國小三年級開始辦理。
                            <span class="indMemo-content--enText">
                                English language education is officially expanded to begin at 3rd grade at elementary schools across the nation.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            發布「教育部推動英語能力檢定測驗處理原則」。
                            <span class="indMemo-content--enText">
                                The MoE issues official guidelines on the use of English examinations for certification purposes.                        
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            教育部繼續「推動高級中學第二外語教育第2期5年計畫」，並成立「推動高級中學第二外語教育推動工作小組」。
                            <span class="indMemo-content--enText">
                                The MoE starts the second phase of its five-year initiative to offer 2nd foreign language education at senior high schools across the nation.
                            </span>
                        </p>      
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2008 -->
    <div class="modLightbox" id="2008">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2008 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            英國蘭卡斯特大學 Charles Alderson教授、美國加州大學洛杉磯分校 Lyle Bachman教授、美國加州州立大學洛杉磯分校Antony Kunnan教授，與澳洲墨爾本大學Tim McNamara教授接受邀請擔任顧問。
                            <span class="indMemo-content--enText">
                                Prof. Charles Alderson from Lancaster Univ., Prof. Lyle Bachman and Prof. Antony Kunnan from UCLA, and Prof. Tim McNamara from the Univ. of Melbourne join the LTTC as consultants for language test development.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">     
                        <img src="images/indimg29.png" alt="" class="indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2009 -->
    <div class="modLightbox" id="2009">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2009 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            開始定期舉辦「LTTC國際學術研討會」。
                            <span class="indMemo-content--enText">
                                The LTTC hosts its first major international academic conference. The LTTC now regularly holds conferences in the fields of language assessment and testing.
                            </span>
                        </p>      
                        <img src="images/indimg30.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            設置GEPT優秀作文獎。
                            <span class="indMemo-content--enText">
                                The LTTC begins an award program for outstanding GEPT Essays.
                            </span>
                        </p>      
                        <img src="images/indimg31.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2010 -->
    <div class="modLightbox" id="2010">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2010 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            GEPT成功爭取國際採認，共獲27國、97校接受作為審核臺灣學生英語能力參考依據。
                            <span class="indMemo-content--enText">
                                The GEPT is internationally recognized: 97 tertiary institutes across 27 countries adopt it as a reliable means of measuring Taiwanese students’ English language ability.
                            </span>
                        </p>      
                        <img src="images/indimg13.png" alt="" class="mt-12 indMemo-content--img">
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            推出GEPT線上能力自我評量系統。
                            <span class="indMemo-content--enText">
                                The LTTC launches the GEPT online self-evaluation system.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            設置LTTC-GEPT研究計劃補助。
                            <span class="indMemo-content--enText">
                                The LTTC establishes the LTTC-GEPT Research Grant program.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            首赴大陸東莞及華東台商子弟學校舉辦GEPT。
                            <span class="indMemo-content--enText">
                                The LTTC administers the GEPT in Dongguan and Huadong, China, at schools for students of Taiwanese business people.
                            </span>
                        </p>      
                        <img src="images/indimg33.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            受教育部委託辦理「中英文翻譯能力檢定考試」。
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MOE to develop and administer Proficiency Tests in Chinese/English Translation and Interpretation.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            GEPT十周年，考生突破420萬人次。
                            <span class="indMemo-content--enText">
                                The LTTC celebrates the GEPT’s 10th Anniversary. The number of  test takers exceeds four million.
                            </span>
                        </p>      
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2014 -->
    <div class="modLightbox" id="2014">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2014 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            為越南外國語大學University of Language and International Studies （ULIS）辦理英語評量研發工作坊。
                            <span class="indMemo-content--enText">
                                The LTTC provides workshops on the development of English assessment for the University of Language and International Studies (ULIS), Vietnam.
                            </span>
                        </p>      
                        <img src="images/indimg34.png" alt="" class="mt-12 indMemo-content--img">
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            呼應教育部政策，啟動第二外語向下扎根計畫，與臺北市多個國小合作，辦理兩期歐日語實驗教學課程。
                            <span class="indMemo-content--enText">
                                The LTTC collaborates with elementary schools in Taipei on piloting courses in Japanese and European languages for kids.
                            </span>
                        </p>      
                        <img src="images/indimg35.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            GEPT考生突破600萬人次。
                            <span class="indMemo-content--enText">
                                The number of GEPT test takers exceeds six million.
                            </span>
                        </p>      
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2017-1 -->
    <div class="modLightbox" id="2017-1">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2017 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            行政院再次啟動「推動英語為官方語」之討論。
                            <span class="indMemo-content--enText">
                                The Executive Yuan re-proposes the idea of stipulating English as an official language in Taiwan.
                            </span>
                        </p>      
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            臺北市擇定兩間公立國小實施雙語實驗課程，採CLIL教學法，每週總授課時數的三分之一以英語授課。
                            <span class="indMemo-content--enText">
                                The Taipei City Government selects two elementary schools to pilot experimental bilingual courses adopting a CLIL approach.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            大學考試入學中心宣布因應108新課綱自111學年度起調整命題。
                            <span class="indMemo-content--enText">
                            The College Entrance Examination Center announces the introduction of new item types in 2022 in response to the 2019 competency-driven curriculum.
                            </span>
                        </p>      
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2017-2 -->
    <div class="modLightbox" id="2017-2">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2017 臺灣外語教育大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            主辦亞洲語言測評研討會AALA & AFELTA，與會人數近300人。
                            <span class="indMemo-content--enText">
                                The LTTC hosts conferences for the Asian Association for Language Assessment and the Academic Forum on English Language Testing in Asia.
                            </span>
                        </p>    
                        <img src="images/indimg36.png" alt="" class="mt-12 indMemo-content--img">
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            越南教育部委託辦理「大規模英語測驗實務工作坊–以全民英檢為例」。
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MoE in Vietnam to conduct workshops on the logistics of administering large-scale English tests—using the GEPT as an example.
                            </span>
                        </p>      
                        <img src="images/indimg37.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            GEPT導入螢幕閱卷評分管理系統。
                            <span class="indMemo-content--enText">
                                The LTTC adopts an automated scoring system for the GEPT.
                            </span>
                        </p>      
                        <img src="images/indimg38.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2019 -->
    <div class="modLightbox" id="2019">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2019 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <img src="images/indimg39.png" alt="" class="mb-12 indMemo-content--img">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            LTTC主編之《亞洲英語能力測驗：在地化與全球化融合之新模式》專書出版。
                            <span class="indMemo-content--enText">
                                A special volume entitled <i>English Language Proficiency Testing: A New Paradigm Bridging Global and Local Contexts</i> is published.
                            </span>
                        </p>    
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            臺北市教育局委託研發國小一、二年級雙語生活課程的聽、說評量。
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Taipei City Government Department of Education to develop listening and speaking assessment tools adopting a CLIL approach for 1st- and 2nd-grade life science courses.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            研發、辦理四階段「素養導向教學與評量」教師增能活動。
                            <span class="indMemo-content--enText">
                                The LTTC offers professional development activities and courses for in-service teachers, focusing on language teaching and assessment.
                            </span>
                        </p>      
                        <img src="images/indimg40.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2020 -->
    <div class="modLightbox" id="2020">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2020 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            新冠肺炎防疫有成，臺灣獨步全球辦理韓語測驗(TOPIK)。
                            <span class="indMemo-content--enText">
                                Thanks to Taiwan’s successful prevention and control of COVID-19, the LTTC becomes the only institute to administer TOPIK during this period.
                            </span>
                        </p>    
                        <img src="images/indimg41.png" alt="" class="mt-12 indMemo-content--img">
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            GEPT二十周年，考生突破800萬人次。
                            <span class="indMemo-content--enText">
                                The LTTC celebrates the GEPT’s 20th Anniversary. The number of test takers exceeds eight million.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            推出「英語素養聯盟」線上課程。
                            <span class="indMemo-content--enText">
                                Smart English Competency Alliance (SECA) online courses are launched.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">    
                        <img src="images/indimg42.png" alt="" class="mt-25 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            臺北市教育局、台大師培中心、北市大全英語教學研究中心委託辦理雙語教育CLIL工作坊，逾400位雙語教師參與
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by several government organizations and educational institutions to provide workshops on Content and Language Integrated Learning (CLIL) for more than 400 bilingual teachers.
                            </span>
                        </p>    
                        <br />  
                        <img src="images/indimg43.png" alt="" class="mt-25 indMemo-content--img" >
                    </article>
                    <article class="indMemo-content">    
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            外交部外交事務及國際學院委託，辦理新進外交領事及行政人員專業英語密集訓練
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Ministry of Foreign Affairs to offer intensive professional English courses for trainees who have passed the civil service examination for diplomats and foreign postings.
                            </span>
                        </p>    
                    </article>
                    <article class="indMemo-content">    
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            臺北市教育局委託規劃設計高中「簡報達人秀」競賽，競賽語種包含歐、日、韓語
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Taipei City Government Department of Education to organize foreign language contests for high school students.
                            </span>
                        </p>    
                        <br />  
                        <img src="images/indimg44.png" alt="" class="mt-25 indMemo-content--img" >
                    </article>
                    <article class="indMemo-content">    
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            為國家文官學院規劃並辦理公務員假日英語工作坊，提供社交英語、簡報英語、會議英語、書信寫作等增能課程
                            <span class="indMemo-content--enText">
                                The LTTC assists the National Civil Service Academy to organize English language workshops to help civil servants improve their professional communication and writing skills.
                            </span>
                        </p>    
                    </article>
                    <article class="indMemo-content">    
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            拍攝有趣生動影片，幫助學習者了解LTTC服務內容
                            <span class="indMemo-content--enText">
                            The LTTC produces educational and entertaining videos to help learners understand the services provided by the LTTC.
                            </span>
                        </p>    
                        <br />  
                        <img src="images/indimg45.png" alt="" class="mt-25 indMemo-content--img" >
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2021 -->
    <div class="modLightbox" id="2021">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2021 LTTC 大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            反映新課綱核心素養，GEPT調整聽讀測驗題型，並基於「學習導向評量」理念推出「GEPT聽診室」，提供考後個人化具體回饋。
                            <span class="indMemo-content--enText">
                                In response to the competency-driven curriculum, the LTTC adjusts the   assessment design of the GEPT’s listening and reading components. Meanwhile, The LTTC also launches the GEPT Diagnosis Room initiative to offer individualized feedback to test takers.
                            </span>
                        </p>    
                        <img src="images/indimg46.png" alt="" class="mt-12 indMemo-content--img">
                    </article>   
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            LTTC成立七十週年、法人成立三十五年。
                            <span class="indMemo-content--enText">
                                The LTTC celebrates its 70th anniversary and its 35th anniversary as a registered educational foundation.
                            </span>
                        </p>      
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            LTTC主編之<i>Rethinking EMI Multidisciplinary Perspectives from Chinese-speaking Regions</i> 與《在地全球化在英語檢定測驗的體現：全民英檢》兩本專書，即將出版。
                            <span class="indMemo-content--enText">
                                Two special volumes, one on EMI in Chinese-speaking regions, the other on GEPT test development, are currently in press.
                            </span>
                        </p>  
                        <img src="images/indimg47.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            為國防部辦理專業英語課程，提升駐外武官英語溝通能力。
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Ministry of Defense to offer English communication courses for military attachés.
                            </span>
                        </p>      
                    </article>
                </article>
            </div>
        </div>
    </div>

</div>